//Socket.io
import io from 'socket.io-client';

//Redux

let store;

export default class ChatroomClient 
{
    /**
         * @param  {Object} data
         * @param  {Object} data.store - The Redux store.
    */
    
    static init(data)
    {
        store = data.store
    }
    
    constructor()
    {
        //  Closed flag.
        // @type {Boolean}
        this._closed = false;
    }

}