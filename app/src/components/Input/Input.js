import React from 'react';

//styles sheet
import styles from './input.module.css';

export default function Input({message, setMessage, sendMessage}) {
    return (
        <div className = {styles.inputContainer}>
            {/* Make the input grow on overflow at later date */}
            <input 
                type = "text" 
                placeholder = "Type a message..." 
                aria-label = "text-input" 
                value = {message}
                className = {styles.input} 
                onChange = {({target: {value}}) => setMessage(value)}
                onKeyPress = {event => event.key === 'Enter' ? sendMessage(event) : null}
            />
            <button className = {styles.send} onClick = {(event) => sendMessage(event)}>send</button>
        </div>
    )
}
