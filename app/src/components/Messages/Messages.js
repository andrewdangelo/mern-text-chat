import React from 'react'

//React scroll to bottom
import ScrollToBottom from 'react-scroll-to-bottom';

//Local imports
import Message from '../Message/Message';

//Styles sheet
import styles from './messages.module.css';

export default function Messages({messages}) {
    return (
        <ScrollToBottom>
            {
                messages.map((message, id) => <div key = {id}><Message /></div>)
            }
        </ScrollToBottom>
    )
}
