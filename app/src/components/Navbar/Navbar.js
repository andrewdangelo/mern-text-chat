import React from 'react';

//react router
import {Link} from 'react-router-dom';

//material-ui
import {Videocam} from '@material-ui/icons';

//Styles
import styles from './navbar.module.css';

export default function Navbar(props) {
    return (
        <div className = {styles.navbar}>
            <button className = {styles.videoButton}>
                <Videocam />
            </button>
            <div className = {styles.titleContainer}>
                <p className = {styles.title}>
                    {props.roomName}
                </p>
            </div>
            <Link to='/'>
                <button className = {styles.leaveButton}>
                    Leave
                </button>
            </Link>
        </div>
    )
}
