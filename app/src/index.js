import React from 'react';
import ReactDOM from 'react-dom';

//Redux
import { Provider } from 'react-redux';
import {
  applyMiddleware,
  createStore
} from 'redux';

//Middlewares
import thunkMiddleware from 'redux-thunk';

//reducer
import rootReducer from '../src/redux/reducers/index'

//Local imports
import ChatroomClient from '../src/components/ChatroomClient';
import Home from './pages/Home';

//Styles
import 'bootstrap/dist/css/bootstrap.min.css';
import * as serviceWorker from './serviceWorker';

const middlewares = [ thunkMiddleware ];

//Create redux store
const store = createStore(rootReducer, undefined, applyMiddleware(...middlewares));

ChatroomClient.init({ store });

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Home />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
