import React, {useState, useEffect} from 'react';

//Module to help retrieve data from the URL
import queryString from 'query-string';

//Add Socket.io client
import io from 'socket.io-client';

//Style sheet
import styles from './chat.module.css';

//Local imports
import Navbar from '../../components/Navbar/Navbar';
import Input from '../../components/Input/Input';
import Messages from '../../components/Messages/Messages';


//Initialize socket
let socket;

export default function Chat({ location }) {
    const [username, setUsername] = useState('');
    const [roomName, setRoomName] = useState('');
    const [message, setMessage] = useState('');
    const [messages, setMessages] = useState([]);

    //Set desired endpoint
    const ENDPOINT = "localhost:5000"

    useEffect(()=>{
        //Extract username and room name from the URL
        const {username, room} = queryString.parse(location.search);

        //Add in socket endpoint
        socket = io(ENDPOINT);


        //Set the room name and username to the local state
        setRoomName(room);
        setUsername(username);
       
        //Socket methods
        socket.emit('join', {username, room});

        //Unmounting the component
        return () => {
            socket.emit('disconnect');

            //Turn off the socket instance
            socket.off();
        }
    },[ENDPOINT, location.search]);



    useEffect(()=>{
        socket.on('msg', (message)=>{
            setMessages(messages => [...messages, message]);
        });
    }, []);

    //Function for sending chat messages
    const sendMessage = (e) => {
        e.preventDefault();

        if(message) {
            socket.emit('sendMsg', message, () => setMessage(''));
        }
    }

    console.log(messages);

    return (
        <div className = {styles.container}>
            <div className = {styles.nav}>
                <Navbar roomName = {roomName}/>
            </div>
            <div className = {styles.messagesContainer}>
                <Messages messages = {messages}/>
            </div>
            <div className = {styles.input}>
                <Input message={message} setMessage={setMessage} sendMessage={sendMessage}/>
            </div>
        </div>
    )
}
