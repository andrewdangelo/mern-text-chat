import React from 'react';

//Connect React-Router
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

//Local imports
import Join from './Join/Join';
import Chat from './Chat/Chat';

function Home() {
  return (
    <Router>
      <Switch>
        <Route exact path = "/" component = {Join} />
        <Route exact path = "/chat" component = {Chat} />
      </Switch>
    </Router>
  );
}

export default Home;
