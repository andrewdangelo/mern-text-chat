import React, {useState} from 'react';
import {Link} from 'react-router-dom';

//add css modules
import styles from './join.module.css'

export default function Join() {
    const [username, setUsername] = useState('');
    const [roomName, setRoomName] = useState('');

    //Get data passed into the input field and set the state
    function handleRoomNameChange(event){
        setRoomName(event.target.value);
    }

    function handleUsernameChange(event){
        setUsername(event.target.value);
    }

    return (
        <div className = {styles.container}>
            <div className = {styles.joinContainer}>
               <div className = {styles.titleContainer}>
                    <h1 className = {styles.title}>Join Room</h1>
                </div>
                <div className = {styles.body}>
                    <form className = {styles.formBody}>
                        <input className = {styles.input} placeholder = "Room name" aria-label = "room-name" onChange = {handleRoomNameChange}/>
                        <input className = {styles.input} placeholder = "User name" aria-label = "user-name" onChange = {handleUsernameChange}/>
                    </form>
                    <div className = {styles.buttonContainer}>
                        {/*Add a error input field message */}
                        <Link onClick = {e => (!username || !roomName) ? e.preventDefault() : null} to = {`/chat?username=${username}&room=${roomName}`}>
                            <button className = {styles.button} type = 'submit'>Go!</button>
                        </Link>
                    </div>
                </div>     
            </div>
        </div>
    )
}
