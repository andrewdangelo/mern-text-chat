import { combineReducers } from 'redux';

import chatroom from './chatroom';

const reducers = combineReducers(
    {
        chatroom,
    }
);

export default reducers;