const express = require('express');
const http = require('http');
const socketIO = require('socket.io');

//import user helper functions
const {addUser, removeUser, getUser, getUsersInARoom} = require('./users');

//Set the port for the server (usually 3000 is reserved from front-end)
const PORT = process.env.PORT || 5000;

//Connect our api endpoints
const router = require('./router');

//Initialize express
const app = express();

//Create a server instance
const server = http.createServer(app);

//Create web socket using instance of the server
const io = socketIO(server);

//Initial user connection
io.on('connect', (socket)=>{
    console.log('User connected');

/* ----------------------------- User has joined ---------------------------- */
    //Catch the 'join' event emitted in Chat.js 
    socket.on('join', ({ username, room }, callback )=>{
        //callback() is a function that can be sent back after the event has been sent
        
        const {error, user} = addUser({ id:socket.id, username, room });

        //check for error
/*         if(error) return callback(error);
 */
        socket.emit('msg', {user: 'admin', text: `${user.username}, welcome to the room ${user.room}!`});
        
        //send message to everyone but the user that sends it
        socket.broadcast.to(user.room).emit('msg', {user: 'admin', text: `${user.username} has joined!`});

        //Join user to room
        socket.join(user.room);
    });

/* -------------------------- User sends a message -------------------------- */
    socket.on('sendMsg', (message, callback) => {
        const user = getUser(socket.id);

        io.to(user.room).emit('msg', {user: user.username, text: message});

        callback();
    });

    //Fired when a user exits the room
    socket.on('disconnect', ()=>{
        console.log("A user has disconnected");
    });
});

//Manage express routers
app.use(router);

//Run the server
server.listen(PORT, ()=>{
    console.log(`Running on port ${PORT}`);
});


