const users = [];

const addUser = ({id, username, room}) => {
    //clean up the room name and username
    username = username.trim().toLowerCase();
    room = room.trim().toLowerCase();

    //Check if a Username is already taken in a room 
    const existingUser = users.find((user)=> user.room === room && user.username === username);

    if (existingUser) {
        return {error: 'Username already taken'};
    }
    const user = {id, username, room};

    users.push(user);

    return { user };
}

const removeUser = (id) =>{
    const index = users.findIndex((user)=> user.id === id);

    if (index !== -1) {
        return users.splice(index, 1)[0];
    }
}

const getUser = (id) => users.find((user) => user.id === id);

const getUsersInARoom = (room) => users.filter((user) => user.room === room);

module.exports = { addUser, removeUser, getUser, getUsersInARoom};